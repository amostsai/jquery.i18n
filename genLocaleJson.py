#!/usr/bin/python
# -*- coding: utf-8 -*-

import re, sys

gettext = { }

# 取得來源檔案中class="gettext"的內容值(content)
def getTargetStrings():
    try:
        f = open(sys.argv[1], 'r')
        text = f.read()
    except:
        print "we can't find your source file."
        sys.exit(-1)

    try:
        iterator = re.finditer(r'class\s*=\s*[\'"]\s*gettext\s*[\'"]>\s*', text)
        if( not iterator ):
            print """we can't find any class="gettext" in your source file."""
            sys.exit(-1)
        else:
            for match in iterator:
                stringEnd = re.search( r'\s*<', text[ match.end(): ] )
                gettext[ text[ match.end() : match.end() + stringEnd.start() ] ] = 1
            print gettext
    except:
        print """we can't parser your source file."""
        sys.exit(-1)


# 根據內容值產生各語言的json檔
def genLocaleJson():
        FileName =  re.match(r'[\w-]*', sys.argv[1]).group()
        print "FileName = %s" % FileName

        if( sys.argv[2:] ):
            for locale in sys.argv[2:]:
                w = open(FileName + '.' + locale + '.json', 'w');
                w.write("$.i18n." + locale + "= { };\n\n")
                w.write("jQuery.i18n." + locale + ".strings = {\n")
                for tag in gettext:
                    w.write('    "' + tag + '": "",\n')
                w.write("\n};\n")
        else:
            print "we can't find your locale name you want to generate"
            sys.exit(-1)


if __name__ == "__main__":
     getTargetStrings()
     genLocaleJson()
else:
    print "mai else"
